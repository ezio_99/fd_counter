from argparse import ArgumentParser
from pathlib import Path
from typing import Set


def get_files_and_subdirectories(directory: Path) -> (Set[Path], Set[Path]):
    """
    Return set of files and subdirectories from given directory.

    :param directory: Directory from which files and subdirectories will be returned.
    :return: Tuple of sets with files and subdirectories.
    """

    files = set()
    subdirectories = set()

    for f in directory.iterdir():
        if f.is_dir():
            subdirectories.add(f)
        elif f.is_file():
            files.add(f)

    return files, subdirectories


def count_files_and_subdirectories(directory: Path, recursively: bool = True) -> (int, int):
    """
    Count files and subdirectories inside of given directory.

    :param directory: Directory to count files and subdirectories.
    :param recursively: Count recursively or not.
    :return: Tuple consisting number of files and subdirectories inside of given directory.
    """

    if recursively:
        files_count = 0
        subdirectories_count = 0
        directories = {directory, }

        while directories:
            directory = directories.pop()
            files, subdirectories = get_files_and_subdirectories(directory)
            directories |= subdirectories

            files_count += len(files)
            subdirectories_count += len(subdirectories)

        return files_count, subdirectories_count

    else:  # no recursion
        files, subdirectories = get_files_and_subdirectories(directory)
        return len(files), len(subdirectories)


def main():
    parser = ArgumentParser(
        usage="Count files and subdirectories inside of given directory. "
              "Also can do it without recursion."
    )
    parser.add_argument(
        "-d",
        "--directory",
        help="Directory to count files and subdirectories",
        type=Path,
        metavar="DIR",
        required=True,
    )
    parser.add_argument(
        "-nr",
        "--no-recursion",
        help="Count without recursion",
        default=False,
        action="store_true",
    )
    args = parser.parse_args()

    directory: Path = args.directory
    recursive: bool = not args.no_recursion

    if not directory.exists():
        print("Directory does not exist")
        exit()

    files_count, subdirectories_count = count_files_and_subdirectories(directory, recursive)
    print(f"{directory.absolute()} contains "
          f"{files_count} files and "
          f"{subdirectories_count} subdirectories"
          f"{' (recursive mode)' if recursive else ''}")


if __name__ == "__main__":
    main()
