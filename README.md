# Info

Program for counting number of files and subdirectories in given directory. Can count in recursive and no-recursive mode.

## Demo

    $ python3 fd_counter.py -d <some_directory>
    <some_directory> contains 1 files and 23 subdirectories (recursive mode)

## Prerequisites

Program written in Python3. Uses `pathlib` module from Python's standard library, therefore requires Python version **3.4** or
higher. If you have no required version of Python, you can download and install it from this
page [https://www.python.org/downloads/](https://www.python.org/downloads/). We recommend disable PATH length limit in Windows and add Python to PATH while you install.

## Libraries

Program uses only standard library, so no additional packages required.

## Usage

Command should be run from terminal. Below commands for different OS.

Linux, MacOS:

    $ python3 fd_counter.py -d <directory_to_count_files_and_subdirectories>

Windows:

    $ python fd_counter.py -d <directory_to_count_files_and_subdirectories>

In case if there are errors with finding executable, or you have several versions of Python installed, specify absolute
path to your Python interpreter:

    $ <path_to_your_python_executable> fd_counter.py -d <directory_to_count_files_and_subdirectories>


Also, you can run program with `-h` flag for getting detailed documentation for every option.

## Features
Additional to recursive mode, you can count files and subdirectories without recursion, just use `-nr` flag.

## Code example

Additional to running program in your terminal, you can reuse counting function in your Python programs. Example:

    from pathlib import Path
    
    from fd_counter import count_files_and_subdirectories

    directory = Path("/")
    files_count, subdirectories_count = count_files_and_subdirectories(directory)

    print(f"{directory} contains {files_count} files and {subdirectories_count} subdirectories")
    # / contains 1 files and 23 subdirectories
